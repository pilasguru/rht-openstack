# Additional material for Openstack RedHat Course

```
NOTE: 
This is not an official distribution from RedHat Certification. 
This is a particular Instructor collection of materials. 
```

## Documentation

It is important to know and understand the RedHat Official documentation of Openstack:

* [Product Documentation for Red Hat OpenStack Platform](https://access.redhat.com/documentation/en/red-hat-openstack-platform/?version=8/)

## Course videos

* [Installation Demonstration: Verifying the Overcloud Installation](https://www.youtube.com/watch?v=_ERGpho0vDs)
* [Installation Demonstration: Installing the Undercloud](https://www.youtube.com/watch?v=ulpxlNFfbF8)
* [Installation Demonstration: Deploying the Overcloud](https://www.youtube.com/watch?v=FF8Ks1aJ_6c)

## Distributed material

* [Openstack Additional](https://gitlab.com/pilasguru/rht-openstack/tree/master/openstack-additional) folder: Additional material to understand cloud computing and Openstack.
* [Openstack Deploy](https://gitlab.com/pilasguru/rht-openstack/tree/master/openstack-deploy) folder: Captures from **different authors/documents** of graphics to deploy openstack.
* [Openstack Course](https://gitlab.com/pilasguru/rht-openstack/tree/master/openstack-course) folder: Additional material about course.

## Author & maintainer

Rodolfo Pilas

* [rodolfo@pilas.guru](mailto:rodolfo@pilas.guru)
* [@pilasguru](https://twitter.com/pilasguru)
* [Blog pilas.guru](https://pilas.guru)
* [Podcast Deployandome](https://deployando.me)
